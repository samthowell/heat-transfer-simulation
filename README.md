# Heat Transfer through tSPL Probe

In thermal scanning probe experiments, one of the most interesting quantities is the temperature at the tip-sample contact. However, in a normal setup as shown in the figure below, this quantity is not *a priori* accessible and has to be calculated. 

![Heat transfer mechanisms from the heater to the sample](img/total-heat-transfer.png)**Figure:** *Schematics for calculating the heat transfer from the heater into the sample*

Neglecting heat transfer through the gap, the tip-sample contact temperature $`T_{contact}`$ can be expressed as:

```math
T_{contact} = \frac{R_{spread}}{R_{tip}+R_{contact}+R_{spread}}(T_{heat}-T_{substrate}) + T_{substrate}
```

where $`T_{heat}`$ and $`T_{substrate}`$ are heater and substrate temperature, respectively. 

In the folloging sections, the three thermal resistances will be derived.

## Tip Thermal Resistance

Following *Gotsmann et al. Nanotechnology 9, Wiley* Page 132-135 the tip thermal resistance can be calculated as follows:

The heat flow $`I_{th}`$ through a rod due to a temperature gradient $`\Delta T/\Delta z`$ is
```math
I_{th} = \frac{\kappa A}{\Delta z}\Delta T,
```
where $`A=\frac{\pi}{4}d^2`$ is the cross-section surface and $`z`$ the coordinate along the rod axis.

Therefore 

```math
I_{th} = \frac{\pi\kappa d^2}{4\Delta z}\Delta T.
```

![Schematics for calculating tip thermal resistance](img/model-tip-thermal-resistance.png)**Figure:** *Schematics for calculating the tip thermal resistance*


For a conical with an opening angle $`\theta`$, the diameter of the tip depends on the distance $`z`$ from the tip apex: $`d(z)=2z \tan(\theta/2)`$. In addition for a diameter smaller than the phonon mean free path  $`\lambda`$, the thermal conductivity is reduced due to boundary scattering of the phonons: $`\kappa(z) \simeq \frac{d(z)}{\lambda} \kappa_{bulk}`$.

Assuming that the cone is made of a stack of $`dz`$ thick discs, reforming and integration gives:

```math
T_1 - T_2 = I_{th}\frac{4}{\pi} \int_{z_1}^{z_2} \frac{1}{\kappa(z) d(z)^2}\mathrm{d}z = I_{th}\frac{4\lambda}{\pi \kappa_b}\int_{z_1}^{z_2} \frac{1}{d(z)^3}\mathrm{d}z
```

Because $`\Delta T = R_{tip} I_{th}`$ and $`\mathrm{d} d(z)/\mathrm{d} z=2 \tan(\theta/2)`$,

```math
R_{tip} = \frac{2\lambda}{\pi \kappa_{bulk} \tan(\theta/2)}\int_{d_0\ll\lambda}^{\lambda} \frac{1}{d^3}\mathrm{d}d = \frac{\lambda}{\pi \kappa_{bulk} \tan(\theta/2)}\left( \frac{1}{d_0^2}-\frac{1}{\lambda^2}\right)
```

where the integration boundaries were chosen as the tip apex diameter $`d_0`$ and the phonon mean free path.

Because $`d_0 \ll \lambda`$,

```math
R_{tip} \simeq \frac{\lambda}{\pi\kappa_b\tan(\theta/2)d_0^2}.
```

### Full Solution for the Tip Thermal Resistance

Analytical caluclation of the tip thermal resistance taking into account diffusive and ballistic thermal transport. [Nelson2008,Gotsmann2010]

The thermal conductivity, similar to the electrical conductivity, in a constriction is reduced due to scattering. Based on Matthiessen's rule, the thermal conductivity can be expressed as:
```math
\kappa(z) = \frac{1}{3}Cv\left(\frac{1}{\lambda}+\frac{1}{d(z)}\right)^{-1}
```

Where C is the volumetric heat capacity, v the average phonon group velocity, $`\lambda`$ the phonon mean free path and d(z) is the diameter of the cone with length L and base diameter D:
```math
d(z) = D/Lz
```

Integrating the thermal resistance over the entire length of the tip ($`L \gg d_0`$)
```math
R_{tip}=\int_{d_0D/L}^{L}\frac{1}{A(z)\kappa(z)}dz 
```
where A(z) is the cross-section surface of the cone:
```math
A(z) = \pi\left(\frac{d(z)}{2}\right)^2
```

Hence: 
```math
R_{tip}=\int_{d_0D/L}^{L}\frac{12(d(z)+\lambda)}{\pi Cv \lambda d(z)^3}dz = \frac{12L^2}{\pi Cv \lambda D^2}\left( (-1)\left.\frac{1}{z}\right\rvert_{d_0L/D}^L + \left(-\frac{1}{2}\right)\frac{L\lambda}{D}\left.\frac{1}{z^2}\right\rvert_{d_0L/D}^L \right)
```

And finally:
```math
R_{tip} = \frac{12L}{\pi Cv \lambda D d_0}\left(1-\frac{d_0}{D}\right) + \frac{6L}{\pi CvDd_0^2}\left(1-\left(\frac{d_0}{D}\right)^2\right)
```
where $`\frac{D}{2L}=\tan(\theta/2)`$.

**Note:** For $`d0\ll D`$ the approximation used by Gotsmann *et al.* can be derived.

<!--
![Tip thermal resistance as a function of apex diameter](img/t_tip-comparison.png)*Tip thermal resistance as a function of apex diameter*
-->



## Tip-Sample Contact Resistance

At the interface between the tip and the substrate, an interfacial contact resistance occurs due to phonon scattering at the interface. [Nelson2008]
The contact resistance can be estimated as:

```math
R_{contact} = \frac{4R_b}{\pi d_{eff}^2}
```

where $`R_b`$ is the thermal boundary resistance and $`d_{eff}`$ the effective contact diameter. $`R_b`$ was found not to vary much between diffent materials at room temperature and typically ranges between $`5 \cdot 10^{-9} - 5 \cdot 10^{-8}`$m<sup>2</sup>K/W.


During thermal indentation, the contact area increased as a function of the penetration depth $`z_{tip}`$. As shown [here](effective_contact_diameter.md), the effective contact diameter of a trucated conical tip can be approximated as


```math
d_{eff}(h) = 2\frac{\sqrt{\sin(\theta/2)}}{\cos(\theta/2)} \sqrt{ h^2 \left[1+\frac{d_0}{h}\left(\frac{1}{\sin(\theta/2)}-1\right)\right] }.
```

## Spreading Resistance

Following [Yovanovich](https://ieeexplore.ieee.org/document/679046) *et al.*, the spreading resistance of a film on a infinite half sphere can be approximated as:

```math
R_{sp} = \frac{1}{2k_1d}-\frac{1}{2\pi k_1 d}\left( \frac{d}{t_1} \right) \ln \left(\frac{2}{1+k_1/k_2} \right), \quad \text{for } 2<t_1/d< \infty
```
where $`k_1`$ is the thermal conductivity of the film with thickness $`t_1`$ and $`k_2`$ the thermal conductivity of the substrate.

Or for thin layers
```math
R_{sp} = \frac{1}{2k_1d}+\frac{1}{\pi k_1 d}\left( \frac{t_1}{d} \right) \left(1-\left( \frac{k_1}{k_2}\right)^2\right), \quad \text{for } 0<t_1/d< 0.10
```


# Overall Heating Efficiency 

The heating efficiency with the different resistances is plotted in the Figure below:

![Heating Efficiency](img/heating-efficiency.png)**Figure:** *Overall heating efficiency of the tip-sample system*


# References

[Nelson2008] https://www.tandfonline.com/doi/abs/10.1080/15567260701866769 

[Gotsmann2010] https://onlinelibrary.wiley.com/doi/full/10.1002/9783527628155.nanotech066

[Yovanovich1998] https://ieeexplore.ieee.org/document/679046
