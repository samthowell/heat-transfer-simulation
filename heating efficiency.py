#!/usr/bin/env python3
"""
Copyright (c) (2019) Samuel Zimmermann <samuel.zimmermann@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import numpy as np
import matplotlib.pylab as plt
from scipy import integrate, special

def R_tip(d0,  theta, kappa_t, lamda, L, model='stzi'):
    """ Tip thermal resistance from different sources
    Sources:
    Gotsmann et al. Nanoscale thermal and mechancial interactions sutdies using heatabl eprobes
    Nelson et al. Nanoscale and microscal ethermophysical Eng. 2008, 12
    
    :d0: tip apex diameter
    :theta: tip opening angle
    :kappa_t: tip thermal conductivity
    :lamda: phonon mean free path in tip material
    :L: tip length
    :model: type of model to use 'stzi', 'gotsmann', 'nelson'
    """
    
    # My model
    if model == 'stzi':
        Cv=1.8e9 # Source Nelson 2008
        return 6/(np.pi*kappa_t*np.tan(theta/2)*d0)*(1-d0/(2*L*np.tan(theta/2)))+3/(np.pi*Cv*np.tan(theta/2)*d0**2)*(1-(d0/(2*L*np.tan(theta/2)))**2)
    
    # Gotsmann
    elif model == 'gotsmann':
        Rs = 1/kappa_t*4*lamda/(3*np.pi*(d0/2)**2)
        return 3/8/np.tan(theta/2)*Rs
    
    # Nelson
    elif model == 'nelson':
        Cv = 1.8e9
        D = 2*np.tan(theta/2)*L
        return 6/(Cv*d0**2)*(1-(d0/D)**2) + 4/(kappa_t*d0)*(1-d0/D)
    else:
        raise NameError('{:s} is an invalid model.'.format(model))
    
    
def R_spread(d, t1, k1, k2, model='stzi', approx='none'):
    """Spreading resistance for a film on an semi-ininite sphere
    Source: Yovanovich et al. IEEE Trans. on Components Packaging and Manuf. Tech. A 1998, 21(1)
    :d: heat source diameter
    :t1: thin film thickness
    :k1: thermal conductivity thin film
    :k2: thermal conductivity substrate
    """
    a = d/2
#    if model == 'nelson':
#
#        def integrand(beta, alpha, t_f, d_eff):
#            return 4/np.pi**(2.5)*np.sin(alpha)*np.sin(beta)*np.tanh(2*t_f/(d_eff*np.sqrt(np.pi))*np.sqrt(alpha**2+beta**2))/np.sqrt(alpha**2+beta**2)
#        
##        return 1/(2*kappa_s*d0)*np.array([integrate.dblquad(integrand, 0, 100, 0, 100, args=(t_f, dd))[0] for dd in d])
#          # use approximation for thick films
#        
#        return 1/(4*k1*a)-1/(2*np.pi*k1*t1)*np.log(2/(1+k1/k2))
        
    if (model == 'stzi') or (model == 'gotsmann') or (model == 'nelson'):
        
        # calculate full solution
        if approx == 'none': 
            def integrand(psi, a, t1, k1, k2):
                """integrand
                """
                K = (1-k2/k1)/(1+k2/k1)
                return (1+K*np.exp(-2*psi*t1/a))/(1-K*np.exp(-2*psi*t1/a))*special.j1(psi)*np.sin(psi)/psi**2
            
            # Variable radius
            if isinstance(a, np.ndarray):
                return 1/(np.pi*k1*a)*np.array([integrate.quad(integrand, 0, np.inf, args=(a_float, t1, k1, k2))[0] for a_float in a])
            # Variable film thickness
            elif isinstance(t1, np.ndarray):
                return 1/(np.pi*k1*a)*np.array([integrate.quad(integrand, 0, np.inf, args=(a, t1_float, k1, k2))[0] for t1_float in t1])
            # TODO: add case for all floats
            else:
                raise TypeError('One argument must be a numpy.array.')
    
        # use approximation for thick films
        elif approx == 'thick':
            return 1/(4*k1*a)-1/(2*np.pi*k1*t1)*np.log(2/(1+k1/k2))
        
        # use approximation for thin films
        elif approx == 'thin':
            return 1/(4*k2*a)+t1/(np.pi*k1*a**2)*(1-(k1/k2)**2)
        
        # use limit for no film
        elif approx == 'no film':
            return 1/(4*k2*a)
        else:
            raise NameError('{:s} is an invalid approximation.'.format(approx))
    else:
        
        raise NameError('{:s} is not a valid model.'.format(model))
        
        
def R_contact(r_b, d_eff, model='stzi'):
    """
    Tip-sample contact resistance according to Ravi prasher, ACS Nano Letters 2005, 5 (11) 2155-2159
    
    Input:
        r_b: Thermal boundary resistance between two materials with different thermal conductivities k1 and k2
        d_eff: effective contact diameter
    """
    # Use stzi model
    if model == 'stzi':
        a_eff = d_eff/2
        return 4*r_b/(np.pi*a_eff**2)
    
    # Use Nelson's model    
    elif model == 'nelson':
        a_eff = d_eff/2
        return 4*r_b/(np.pi*a_eff**2)
    
    # Use Gotsmann's model
    elif model == 'gotsmann':
#        TODO: something wrong with these formula
#        def R_sp(kappa_s, d0):
#            """
#            Spreading resistance
#            kappa_s: substrate thermal conductivity
#            d0: tip apex diameter
#            """
#            return 1/(2*kappa_s*d0)
#        
#        
#        def R_b(r_tip, r_sp, theta, a):
#            """
#            Boundary resistance
#            r: tip thermal resistance
#            t_sp: spreading resistance
#            theta: opening angle
#            a: accommodation factor for the back-scattering of the tphonons into the tip
#            """
#            eta = a*(1-1/np.sqrt(4*np.tan(theta/2)**2+1))
#
#            return eta/(1-eta)*(r_tip + r_sp)
#        
#        
#        def R_tip(d0, theta, lamda, kappa_t):
#            """
#            Tip thermal resistance
#            d0: tip apex diameter
#            theta: tip opening angle
#            lamda: phonon mean free path
#            kappa_t: tip material thermal conductivity
#            """
#            Rs = 4*lamda/(kappa_t*3*np.pi*(d0/2)**2)
#            return 3*Rs/(8*np.tan(theta/2))
#        
#        
#        def R_int(r_b, r_tip):
#            """
#            interface thermal resistance
#            R_b: boundary thermal resistance
#            T_tip: tip thermal resistance
#            """
#            return r_b+r_tip
#        
#        r_sp = R_sp(kappa_s, d_eff)
#        r_tip = R_tip(d0, theta, lamda, kappa_t)
#        r_b = R_b(r_tip, r_sp, theta, 0.75)
#        return R_int(r_b, r_tip)
        a = 0.75
        d0=d_eff
        from numpy import sqrt, tan, pi
        return (pi*a*d0*kappa_t*sqrt(4*tan(theta/2)**2 + 1) - pi*a*d0*kappa_t + 4*kappa_s*lamda*sqrt(4*tan(theta/2)**2 + 1)/tan(theta/2))/(2*pi*d0**2*kappa_s*kappa_t*(-a*sqrt(4*tan(theta/2)**2 + 1) + a + sqrt(4*tan(theta/2)**2 + 1)))

        
    else:
        raise NameError('{:s} is an invalid model.'.format(model))
        
def D_eff(theta, z_tip, d0, model='stzi'):

    if model == 'trunc_cone':
        h0 = d0/2*(1/np.sin(theta/2)-1)
        return 2*np.sqrt(np.sin(theta/2))/np.cos(theta/2)*np.sqrt((z_tip+h0)**2-h0**2)
    
    elif model == 'gotsmann':
        return d0*np.ones(z_tip.shape)

    elif model == 'nelson':
        return np.sqrt(4*z_tip*d0)*2


def C_heating(r_tip, r_spread, r_contact):
    return r_spread/(r_tip+r_spread+r_contact)



# ------------------------------ Figure ---------------------------------------
fig, axs = plt.subplots(2,2,figsize=(6.7,6.5),squeeze=False)
print(axs)
my_plot_rtip = axs[0][0].axhline
my_plot_rspread = axs[0][1].loglog
my_plot_rcontact = axs[1][0].loglog
my_plot_c = axs[1][1].plot
titles = [['Rtip', 'Rspread'],['Rcontact', 'c']]
colors = ['tab:green', 'tab:blue', 'black']
linestyles = ['-', '--', ':']
xlabels = [['-', 'Penetration Depth (nm)'],['Penetration Depth (nm)', 'Penetration Depth (nm)']]
ylabels = [['Rtip (K/W)', 'Rspread (K/W)'],['Rcontact (nm)', 'Heating Efficiency']]



#ax.set_xlabel('Penetration Depth (m)')
#ax.set_ylabel('Heating Efficiency (-)')

# ------------------------------ Constants ------------------------------------
R_SPREAD_APPROX = 'thick'    # Approximation for Rspread: 'none', 'thick', 'thin', 'no film'
#MODEL = 'nelson'
models = ['nelson', 'gotsmann', 'stzi']    # Models 'stzi', 'nelson', gotsmann'

# ------------------------- Material Constants --------------------------------
kappa_f = 0.9               # Thermal conductivity polymer film
kappa_s = 95                # Thermal conductivity substrate
kappa_t = 95                # Thermal conductivity tip
lamda = 70e-9               # Phonon mean free path in tip material
#a_accomm⎄3odation = 0.75      # Accomodation factor for Gotsmann model

# ------------------------- Geometry Constants --------------------------------
t_f = 300e-9                  # Polymer film thickness
theta = 30/180*np.pi        # Tip opening angle
l = 500e-9                    # Tip length
d0 = 10e-9                   # Tip apex diameter


# ------------------------- Variables -----------------------------------------
z_tip_arr = np.linspace(1e-9, 100e-9, 100)   # Tip indentation depth
r_b_arr = np.array([5e-9])#,5e-8, , 2e-9 1e-8, 5e-8])      # Boundary resistance




# --------------------- Loop over boundary resistance -------------------------
for i, MODEL in enumerate(models):
#    print('Model {:s}'.format(MODEL))
    for j, r_b in enumerate(r_b_arr):
#        print('Rb: {:.1e}'.format(r_b))
        
        # Effective contact diameter
        d_eff_arr = D_eff(theta, z_tip_arr, d0, model = 'trunc_cone')
#       d_eff_arr = z_tip_arr
        
        r_tip = R_tip(d0, theta, kappa_t, lamda, l, model=MODEL)
#        r_tip = 1e7
        r_spread = R_spread(d_eff_arr, t_f, kappa_f, kappa_s, model=MODEL, approx=R_SPREAD_APPROX)
        r_contact = R_contact(r_b, d_eff_arr, model = MODEL)
#        print(r_spread)
#        print(r_contact)
#        print(r_tip)
        c = C_heating(r_tip, r_spread, r_contact)
        
        my_plot_rtip(r_tip, label='{:.0e} {:s}'.format(r_b, MODEL), color=colors[i], linestyle=linestyles[j])
        my_plot_rspread(z_tip_arr*1e9, r_spread, label='{:.0e} {:s}'.format(r_b, MODEL), color=colors[i], linestyle=linestyles[j])
        my_plot_rcontact(z_tip_arr*1e9, r_contact, label='{:.0e} {:s}'.format(r_b, MODEL), color=colors[i], linestyle=linestyles[j])
        my_plot_c(z_tip_arr*1e9, c, label='{:.0e} {:s}'.format(r_b, MODEL), color=colors[i], linestyle=linestyles[j])


# ------------------------- Plotting ------------------------------------------
[[axs[i][j].set_xlabel(xlabels[i][j]) for i in range(len(axs[j]))]  for j in range(len(axs))]
[[axs[i][j].set_ylabel(ylabels[i][j]) for i in range(len(axs[j]))]  for j in range(len(axs))]
[[axs[i][j].set_title(titles[i][j]) for i in range(len(axs[j]))]  for j in range(len(axs))]
#[[axs[i][j].legend(frameon=False) for i in range(len(axs[j]))] for j in range(len(axs))]


#import matplotlib.lines as mlines
#handles = []
#for i in range(len(models)):
#    line = mlines.Line2D([], [], color=colors[i], linestyle=linestyles[0], label='{:s}'.format(models[i]))
#    handles.append(line)    
#for j in range(len(r_b_arr)):
#    line = mlines.Line2D([], [], color='k', linestyle=linestyles[i], label='{:.0e}'.format(r_b_arr[j]))
#    handles.append(line)
#
#text=models + ['{:.0e}'.format(r_b) for r_b in r_b_arr]
#axs[0][0].legend(handles, text)
axs[0][0].legend()
plt.tight_layout()
plt.savefig('heating-efficiency.png')
plt.show()
